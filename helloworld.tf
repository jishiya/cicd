provider "aws" {
  region = "ap-south-1"
}

provider "archive" {}
data "archive_file" "zip" {
  type        = "zip"
  source_file = "helloworld.py"
  output_path = "helloworld.zip"
}

terraform {
  backend "s3" {
    bucket = "jishiya-lambda-backend"
    key = "statefile/terraform.tfstate"
    region = "ap-south-1"
  }
}

data "aws_iam_policy_document" "policy" {
  statement {
    sid    = ""
    effect = "Allow"
    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }
    actions = ["sts:AssumeRole"]
  }
}

#resource "aws_iam_role" "iam_for_lambda" {
#  name = "iam_for_lambda"
#
#  assume_role_policy = <<EOF
#{
#  "Version": "2012-10-17",
#  "Statement": [
#    {
#      "Action": "sts:AssumeRole",
#      "Principal": {
#        "Service": "lambda.amazonaws.com"
#      },
#      "Effect": "Allow",
#      "Sid": ""
#    }
#  ]
#}
#EOF
#}

resource "aws_iam_role" "iam_for_lambda" {
  name               = "iam_for_lambda"
  assume_role_policy = data.aws_iam_policy_document.policy.json
}

resource "aws_lambda_function" "test_lambda" {
  filename      = "helloworld.zip"
  function_name = "helloworld"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "helloworld.lambda_handler"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  //source_code_hash = filebase64sha256("lambda_function_payload.zip")

  runtime = "python3.9"

  /*environment {
    variables = {
      foo = "bar"
    }
  }*/
}

data "aws_lambda_invocation" "example" {
  function_name = aws_lambda_function.test_lambda.function_name

/*  input = <<JSON
{
  "key1": "value1",
  "key2": "value2"
}
JSON*/
  input = ""
}

output "result_entry" {
  value = jsondecode(data.aws_lambda_invocation.example.result)
}


