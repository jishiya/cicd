import json

def lambda_handler(event, context):
    # TODO implement
    return {
        'message': "Hello World from lambda",
        'responseCode': 200,
        'body': json.dumps('CICD pipeline is completed!')
    }
